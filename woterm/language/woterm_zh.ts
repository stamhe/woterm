<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>English</name>
    <message>
        <location filename="../qwosetting.cpp" line="162"/>
        <source>English</source>
        <translation>简体中文</translation>
    </message>
</context>
<context>
    <name>QKxConfirmWidget</name>
    <message>
        <location filename="../qkxconfirmwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qkxconfirmwidget.ui" line="27"/>
        <location filename="../qkxconfirmwidget.ui" line="50"/>
        <source>TextLabel</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../qkxconfirmwidget.ui" line="79"/>
        <source>Yes</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../qkxconfirmwidget.ui" line="86"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Please input password</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <source>Failed to authentication: %1</source>
        <translation type="vanished">鉴权失败</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="54"/>
        <source>Auto mode</source>
        <translation>自动模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="56"/>
        <source>Lossless mode</source>
        <translation>无损模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="57"/>
        <source>TureColor mode</source>
        <translation>真彩色模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="59"/>
        <source>HDTV mode</source>
        <translation>高清模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="60"/>
        <source>General clear mode</source>
        <translation>普清模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="61"/>
        <source>Fast mode</source>
        <translation>流畅模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="62"/>
        <source>Super fast mode</source>
        <translation>极速模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="63"/>
        <source>Classics 16bit mode</source>
        <translation>经典16位模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="64"/>
        <source>Classics 15bit mode</source>
        <translation>经典15位模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="65"/>
        <source>Classics 8bit mode</source>
        <translation>经典8位模式</translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.cpp" line="66"/>
        <source>Classics pallete mode</source>
        <translation>经典调色板模式</translation>
    </message>
</context>
<context>
    <name>QRLoginPtyClient</name>
    <message>
        <location filename="../qworlogin.cpp" line="347"/>
        <source>Failed to bind to local port for no permission, switch to root user and try it again?</source>
        <translation>没有权限绑定本地端口，请切换至超级用户权限</translation>
    </message>
    <message>
        <location filename="../qworlogin.cpp" line="349"/>
        <source>Failed to bind to local port for unknow reasion</source>
        <translation>无法绑定本地端口，原因未知。</translation>
    </message>
    <message>
        <location filename="../qworlogin.cpp" line="351"/>
        <source>Failed to connect to remote server</source>
        <translation>无法连接远程服务器</translation>
    </message>
</context>
<context>
    <name>QSshAuthClient</name>
    <message>
        <source>Failed to connect server : %1</source>
        <translation type="vanished">无法连接服务：%1</translation>
    </message>
</context>
<context>
    <name>QWoAboutDialog</name>
    <message>
        <location filename="../qwoaboutdialog.ui" line="20"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.ui" line="28"/>
        <source>Current version:</source>
        <translation>当前版本：</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.ui" line="52"/>
        <source>Latest version:</source>
        <translation>最新版本：</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.ui" line="76"/>
        <source>Offcie website:</source>
        <translation>官网：</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.ui" line="119"/>
        <source>Check Version</source>
        <translation>检查版本</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.cpp" line="69"/>
        <location filename="../qwoaboutdialog.cpp" line="81"/>
        <location filename="../qwoaboutdialog.cpp" line="86"/>
        <source>version check</source>
        <translation>版本检查</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.cpp" line="69"/>
        <source>Failed For %1</source>
        <translation>失败原因</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.cpp" line="81"/>
        <source>Found New Version: %1, Try To Download?</source>
        <translation>发现新版本：%1，现在去下载？</translation>
    </message>
    <message>
        <location filename="../qwoaboutdialog.cpp" line="86"/>
        <source>No New Version</source>
        <translation>已经是最新版本</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">标签</translation>
    </message>
</context>
<context>
    <name>QWoBaseToolForm</name>
    <message>
        <location filename="../qwobasetoolform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwobasetoolform.ui" line="35"/>
        <location filename="../qwobasetoolform.ui" line="52"/>
        <location filename="../qwobasetoolform.ui" line="69"/>
        <location filename="../qwobasetoolform.ui" line="86"/>
        <location filename="../qwobasetoolform.ui" line="103"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QWoCommandLineInput</name>
    <message>
        <location filename="../qwocommandlineinput.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>QWoDBRestoreDialog</name>
    <message>
        <location filename="../qwodbrestoredialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.ui" line="20"/>
        <source>Backup file path:</source>
        <translation>备份文件路径：</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.ui" line="62"/>
        <source>Restore</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="32"/>
        <source>Database Restore</source>
        <translation>数据库恢复</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="48"/>
        <source>Restore Session Database</source>
        <translation>恢复数据库</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="59"/>
        <location filename="../qwodbrestoredialog.cpp" line="63"/>
        <location filename="../qwodbrestoredialog.cpp" line="68"/>
        <location filename="../qwodbrestoredialog.cpp" line="70"/>
        <source>Restore information</source>
        <translation>恢复信息</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="59"/>
        <source>please select a backup file to restore</source>
        <translation>选择一个备份文件去恢复</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="63"/>
        <source>The backup file is incorrect or corrupt</source>
        <translation>备份文件已经损坏或格式不符合要求。</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="68"/>
        <source>success to restore database.</source>
        <translation>数据库恢复成功。</translation>
    </message>
    <message>
        <location filename="../qwodbrestoredialog.cpp" line="70"/>
        <source>failed to restore database.</source>
        <translation>数据库恢复失败。</translation>
    </message>
</context>
<context>
    <name>QWoHostInfo</name>
    <message>
        <location filename="../qwohostinfo.ui" line="17"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="39"/>
        <source>Name:</source>
        <translation>名字：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="53"/>
        <source>Host:</source>
        <translation>主机名：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="67"/>
        <source>Port:</source>
        <translation>端口：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="94"/>
        <source>UserName:</source>
        <translation>用户名：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="131"/>
        <source>Login:</source>
        <translation>类型：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="139"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="144"/>
        <source>IdentifyFile</source>
        <translation>证书</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="169"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="200"/>
        <source>IdentifyFile:</source>
        <translation>证书：</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="220"/>
        <location filename="../qwohostinfo.ui" line="254"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="231"/>
        <source>ProxyJump</source>
        <translation>跳板机</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="265"/>
        <source>Memo</source>
        <translation>备忘</translation>
    </message>
    <message>
        <location filename="../qwohostinfo.ui" line="292"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
</context>
<context>
    <name>QWoHostInfoEdit</name>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="30"/>
        <location filename="../qwohostinfoedit.cpp" line="157"/>
        <source>Add</source>
        <translation>增加</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="39"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="63"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="82"/>
        <location filename="../qwohostinfoedit.cpp" line="86"/>
        <location filename="../qwohostinfoedit.cpp" line="90"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="82"/>
        <source>The name can&apos;t be empty</source>
        <translation>名字不能为空</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="86"/>
        <source>The host can&apos;t be empty</source>
        <translation>主机名不能为空</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="90"/>
        <source>The port should be at [10,65535]</source>
        <translation>端口范围必须在[10,65535]之间</translation>
    </message>
    <message>
        <location filename="../qwohostinfoedit.cpp" line="146"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
</context>
<context>
    <name>QWoHostInfoList</name>
    <message>
        <location filename="../qwohostinfolist.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
</context>
<context>
    <name>QWoHostList</name>
    <message>
        <location filename="../qwohostlist.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwohostlist.ui" line="43"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>QWoHostListModel</name>
    <message>
        <location filename="../qwohostlistmodel.cpp" line="113"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../qwohostlistmodel.cpp" line="115"/>
        <source>Host</source>
        <translation>主机名</translation>
    </message>
    <message>
        <location filename="../qwohostlistmodel.cpp" line="117"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../qwohostlistmodel.cpp" line="119"/>
        <source>Memo</source>
        <translation>备忘</translation>
    </message>
</context>
<context>
    <name>QWoHostSimpleList</name>
    <message>
        <location filename="../qwohostsimplelist.cpp" line="29"/>
        <source>jump list</source>
        <translation>跳板机列表</translation>
    </message>
</context>
<context>
    <name>QWoHostTreeModel</name>
    <message>
        <location filename="../qwohosttreemodel.cpp" line="157"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../qwohosttreemodel.cpp" line="159"/>
        <source>Host</source>
        <translation>主机名</translation>
    </message>
    <message>
        <location filename="../qwohosttreemodel.cpp" line="161"/>
        <source>Memo</source>
        <translation>备忘</translation>
    </message>
</context>
<context>
    <name>QWoIdentifyCreateDialog</name>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="vanished">类型：</translation>
    </message>
    <message>
        <source>Bits:</source>
        <translation type="vanished">比特位数</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="20"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="26"/>
        <source>ED25519(recommend)</source>
        <translation>ED25519</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="33"/>
        <source>RSA</source>
        <translation>RSA</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="43"/>
        <source>Bits</source>
        <translation>位数</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="49"/>
        <source>2048(recommend)</source>
        <translation>2048</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="56"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="68"/>
        <source>Name:</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.ui" line="95"/>
        <source>Create</source>
        <translation>创建</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.cpp" line="32"/>
        <source>IdentifyCreate</source>
        <translation>创建证书</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.cpp" line="60"/>
        <location filename="../qwoidentifycreatedialog.cpp" line="71"/>
        <source>info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.cpp" line="60"/>
        <source>failed to create identify:[%1]</source>
        <translation>无法创建证书[%1]</translation>
    </message>
    <message>
        <location filename="../qwoidentifycreatedialog.cpp" line="71"/>
        <source>failed to save identity for name already exist:[%1]</source>
        <translation>无法保存相同名字的证书文件：[%1]</translation>
    </message>
</context>
<context>
    <name>QWoIdentifyDialog</name>
    <message>
        <location filename="../qwoidentifydialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="35"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="42"/>
        <source>Create</source>
        <translation>创建</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="49"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="56"/>
        <source>PublicKey</source>
        <translation>公钥</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="63"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="70"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="77"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.ui" line="84"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="43"/>
        <source>Identify Manage</source>
        <translation>证书管理</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="55"/>
        <source>name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="56"/>
        <source>type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="57"/>
        <source>fingerprint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="92"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="99"/>
        <location filename="../qwoidentifydialog.cpp" line="103"/>
        <location filename="../qwoidentifydialog.cpp" line="108"/>
        <location filename="../qwoidentifydialog.cpp" line="119"/>
        <location filename="../qwoidentifydialog.cpp" line="149"/>
        <location filename="../qwoidentifydialog.cpp" line="180"/>
        <location filename="../qwoidentifydialog.cpp" line="197"/>
        <location filename="../qwoidentifydialog.cpp" line="210"/>
        <location filename="../qwoidentifydialog.cpp" line="231"/>
        <location filename="../qwoidentifydialog.cpp" line="238"/>
        <source>info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="99"/>
        <source>Public key file import is not supported. Please select a valid private key file.</source>
        <translation>不支持公钥文件导入，请选择一个有效的私钥文件。</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="103"/>
        <source>Invalid private key file. Please select a valid private key file.</source>
        <translation>无效的私钥文件，请选择一个有效的私钥文件。</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="108"/>
        <source>the identify&apos;s file is bad</source>
        <translation>证书文件已经损坏</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="119"/>
        <source>the same identify had been exist by name: %1</source>
        <translation>已经存在相同的证书</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="140"/>
        <source>The name already exists. Please enter a new name.</source>
        <translation>已经存在相同的名字，请输入新的名字。</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="149"/>
        <location filename="../qwoidentifydialog.cpp" line="180"/>
        <location filename="../qwoidentifydialog.cpp" line="197"/>
        <location filename="../qwoidentifydialog.cpp" line="210"/>
        <location filename="../qwoidentifydialog.cpp" line="231"/>
        <location filename="../qwoidentifydialog.cpp" line="238"/>
        <source>no selection</source>
        <translation>没有选中选项</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="187"/>
        <source>failed to delete %1</source>
        <translation>无法删除%1</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="221"/>
        <source>failed to rename &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>无法重新命名&apos;%1&apos;至&apos;%2&apos;</translation>
    </message>
    <message>
        <source>the identify&apos;s file is not exist</source>
        <translation type="vanished">证书文件不存在</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="156"/>
        <source>Save File</source>
        <translation>保存文件</translation>
    </message>
    <message>
        <location filename="../qwoidentifydialog.cpp" line="187"/>
        <location filename="../qwoidentifydialog.cpp" line="221"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>failed to delete file:%1</source>
        <translation type="vanished">无法删除目标文件</translation>
    </message>
    <message>
        <source>failed to rename file:[%1]-&gt;[%2]</source>
        <translation type="vanished">无法重新命名文件：[%1]-&gt;[%2]</translation>
    </message>
</context>
<context>
    <name>QWoIdentifyPublicKeyDialog</name>
    <message>
        <location filename="../qwoidentifypublickeydialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwoidentifypublickeydialog.ui" line="20"/>
        <source>Copy the follow public key to remote server</source>
        <translation>复制公钥内容至远程服务器</translation>
    </message>
    <message>
        <location filename="../qwoidentifypublickeydialog.ui" line="45"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qwoidentifypublickeydialog.ui" line="52"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../qwoidentifypublickeydialog.cpp" line="24"/>
        <source>Public Key</source>
        <translation>公钥</translation>
    </message>
</context>
<context>
    <name>QWoMainWindow</name>
    <message>
        <location filename="../qwomainwindow.ui" line="14"/>
        <source>QWoMainWindow</source>
        <translation>QWoMainWindow</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="41"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="106"/>
        <source>Tool Bar</source>
        <translation>工具栏</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="111"/>
        <source>Session List</source>
        <translation>会话列表</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="116"/>
        <location filename="../qwomainwindow.cpp" line="405"/>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="151"/>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="156"/>
        <source>Restore</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="131"/>
        <source>Identity Manage</source>
        <translation>证书管理</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="141"/>
        <source>Online Document</source>
        <translation>在线文档</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="146"/>
        <source>Office Wetsite</source>
        <translation>官网</translation>
    </message>
    <message>
        <source>Session Backup</source>
        <translation type="vanished">备份</translation>
    </message>
    <message>
        <source>Session Restore</source>
        <translation type="vanished">恢复</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">编辑</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="53"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="60"/>
        <source>Tool</source>
        <translation>工具</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">选项卡</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="67"/>
        <location filename="../qwomainwindow.cpp" line="409"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="126"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="101"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="vanished">导入</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">导出</translation>
    </message>
    <message>
        <source>Log</source>
        <translation type="vanished">日志</translation>
    </message>
    <message>
        <source>Transfer</source>
        <translation type="vanished">传输</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">断开</translation>
    </message>
    <message>
        <source>Reconnect</source>
        <translation type="vanished">重新连接</translation>
    </message>
    <message>
        <source>Reconnect All</source>
        <translation type="vanished">全部重新连接</translation>
    </message>
    <message>
        <source>Default Property</source>
        <translation type="vanished">默认属性</translation>
    </message>
    <message>
        <source>Find..</source>
        <translation type="vanished">查找...</translation>
    </message>
    <message>
        <source>version check</source>
        <translation type="vanished">版本检查</translation>
    </message>
    <message>
        <source>Found New Version: %1,Try To Download?</source>
        <translation type="vanished">发现新版本：%1，现在去下载？</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="59"/>
        <source>WoTerm</source>
        <translation>WoTerm</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="65"/>
        <source>Session Manager</source>
        <translation>会话管理</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="121"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="121"/>
        <source>Exit Or Not?</source>
        <translation>退出或取消</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="259"/>
        <source>Version check</source>
        <translation>版本检查</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="259"/>
        <source>a new version of %1 is found, do you want to update it?</source>
        <translation>发现新版本%1，是否需要更新？</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="288"/>
        <source>Backup Session Database</source>
        <translation>备份数据库</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="297"/>
        <source>Failure</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="297"/>
        <source>failed to backup the session list.</source>
        <translation>备份数据库失败。</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="331"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="331"/>
        <source>The language has been changed, restart application to take effect right now.</source>
        <translation>语言已经被修改，需要重启才能生效，是否立即重启？</translation>
    </message>
    <message>
        <source>The Language has been changed, Please restart application to take effect.</source>
        <translation type="vanished">语言已经被修改，应用重启才生效</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="121"/>
        <location filename="../qwomainwindow.cpp" line="398"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="399"/>
        <source>Manage</source>
        <translation>管理</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="400"/>
        <source>List</source>
        <translation>列表</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.cpp" line="407"/>
        <source>Keys</source>
        <translation>证书管理</translation>
    </message>
    <message>
        <location filename="../qwomainwindow.ui" line="136"/>
        <location filename="../qwomainwindow.cpp" line="410"/>
        <source>About</source>
        <translation>关于...</translation>
    </message>
</context>
<context>
    <name>QWoPasswordInput</name>
    <message>
        <location filename="../qwopasswordinput.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.ui" line="28"/>
        <source>TextLabel</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.ui" line="48"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.ui" line="73"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.ui" line="83"/>
        <source>Visible</source>
        <translation>显示</translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.ui" line="120"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.cpp" line="65"/>
        <source>Tip</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../qwopasswordinput.cpp" line="65"/>
        <source>The Password is Empty, continue to finish?</source>
        <translation>当前密码为空，是否继续执行？</translation>
    </message>
</context>
<context>
    <name>QWoPowerSftp</name>
    <message>
        <location filename="../qwossh.cpp" line="256"/>
        <source>An attempt to create an already existing file or directory has been made</source>
        <translation>已经存在同名的目标文件或目录</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="260"/>
        <source>No such file or directory path exists</source>
        <translation>目标文件或目录不存在</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="263"/>
        <source>Permission denied</source>
        <translation>没有权限</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="266"/>
        <source>Generic failure</source>
        <translation>常规性失败</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="269"/>
        <source>Garbage received from server</source>
        <translation>接收到与协议不符合的内容</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="272"/>
        <source>No connection has been set up</source>
        <translation>还没有建立连接</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="275"/>
        <source>There was a connection, but we lost it</source>
        <translation>连接已经断开</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="278"/>
        <source>Operation not supported by the server</source>
        <translation>服务端不支持该操作</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="281"/>
        <source>Invalid file handle</source>
        <translation>无效的文件句柄</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="284"/>
        <source>We are trying to write on a write-protected filesystem</source>
        <translation>文件已经被系统保护，无法写入任何内容</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="287"/>
        <source>No media in remote drive</source>
        <translation>远程服务器没有相关文件</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="456"/>
        <source>Skip it for the local file had exist.</source>
        <translation>本地文件已经存在，放弃该操作</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="461"/>
        <source>Failed to open the local file.</source>
        <translation>无法打开本地文件</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="515"/>
        <source>Failed to open local file.</source>
        <translation>无法打开本地文件</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="617"/>
        <source>Error allocating SFTP session: %1</source>
        <translation>会话不存在：%1</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="621"/>
        <source>Error initializing SFTP session: %1</source>
        <translation>会话初始化失败：%1</translation>
    </message>
    <message>
        <location filename="../qwossh.cpp" line="680"/>
        <source>the last request is still running, wait for a while.</source>
        <translation>还没有完成上一次操作，请继续等待</translation>
    </message>
</context>
<context>
    <name>QWoRLoginTermWidget</name>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="196"/>
        <location filename="../qworlogintermwidget.cpp" line="207"/>
        <source>session list</source>
        <translation>会话列表</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="238"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="238"/>
        <source>can&apos;t find the session, maybe it had been delete ago</source>
        <translation>会话信息已经不存在</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="257"/>
        <source>Select Files</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="276"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="436"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="438"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="440"/>
        <source>Force Reconnect</source>
        <translation>强制重新联网</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="441"/>
        <source>Split Vertical</source>
        <translation>垂直分割</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="443"/>
        <source>Split Horizontal</source>
        <translation>水平分割</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="449"/>
        <source>Close Session</source>
        <translation>关闭会话</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="454"/>
        <source>Float This Tab</source>
        <translation>浮动此选项卡</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="457"/>
        <source>Find...</source>
        <translation>查找...</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="458"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="460"/>
        <source>Duplicate In New Window</source>
        <translation>复制会话至新窗口</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="465"/>
        <source>Zmodem Upload</source>
        <translation>ZModem上传</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="466"/>
        <source>Zmodem Receive</source>
        <translation>ZModem下载</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="467"/>
        <source>Zmoddem Abort</source>
        <translation>ZModem中止传输</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="488"/>
        <source>Files are transfering...</source>
        <translation>文件正在传输中...</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="461"/>
        <source>Clean history</source>
        <translation>清空历史</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="462"/>
        <source>Output history to file</source>
        <translation>将历史输出至文件</translation>
    </message>
    <message>
        <location filename="../qworlogintermwidget.cpp" line="463"/>
        <source>Stop history to file</source>
        <translation>停止输出历史文件</translation>
    </message>
</context>
<context>
    <name>QWoRLoginTermWidgetImpl</name>
    <message>
        <location filename="../qworlogintermwidgetimpl.cpp" line="31"/>
        <source>%1:%2</source>
        <translation>%1:%2</translation>
    </message>
</context>
<context>
    <name>QWoRenameDialog</name>
    <message>
        <location filename="../qworenamedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qworenamedialog.ui" line="28"/>
        <source>Name:</source>
        <translation>名字：</translation>
    </message>
    <message>
        <location filename="../qworenamedialog.ui" line="53"/>
        <source>TextLabel</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../qworenamedialog.ui" line="90"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qworenamedialog.ui" line="97"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qworenamedialog.cpp" line="50"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
</context>
<context>
    <name>QWoSerialInput</name>
    <message>
        <location filename="../qwoserialinput.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="52"/>
        <source>COM:</source>
        <translation>串口：</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="79"/>
        <source>Auto new line</source>
        <translation>自动换行</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="86"/>
        <source>Text mode</source>
        <translation>文本模式</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="116"/>
        <source>Reflesh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="133"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="167"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="184"/>
        <source>Disconnect</source>
        <translation>中断连接</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="200"/>
        <source>Failed to find any com device.</source>
        <translation>没有串口设备</translation>
    </message>
    <message>
        <location filename="../qwoserialinput.ui" line="255"/>
        <source>send</source>
        <translation>发送</translation>
    </message>
</context>
<context>
    <name>QWoSerialTermWidget</name>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="339"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="341"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="363"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="363"/>
        <source>can&apos;t find the session, maybe it had been delete ago</source>
        <translation>没有找到指定会话的信息</translation>
    </message>
</context>
<context>
    <name>QWoSerialWidgetImpl</name>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="132"/>
        <location filename="../qwoserialwidgetimpl.cpp" line="176"/>
        <location filename="../qwoserialwidgetimpl.cpp" line="204"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="132"/>
        <location filename="../qwoserialwidgetimpl.cpp" line="204"/>
        <source>can&apos;t find the session, maybe it had been delete</source>
        <translation>没有找到对应的会话</translation>
    </message>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="176"/>
        <source>Failed to open device.</source>
        <translation>没有打开设备</translation>
    </message>
    <message>
        <location filename="../qwoserialwidgetimpl.cpp" line="232"/>
        <source>An I/O error occurred from port %1, error: %2</source>
        <translation>在串口%1发生输入输出错误：%2</translation>
    </message>
</context>
<context>
    <name>QWoSessionList</name>
    <message>
        <location filename="../qwosessionlist.cpp" line="79"/>
        <source>Enter keyword to search</source>
        <translation>关键字搜索</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="168"/>
        <source>name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="169"/>
        <source>host</source>
        <translation>主机名</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="170"/>
        <source>memo</source>
        <translation>备忘</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="335"/>
        <location filename="../qwosessionlist.cpp" line="340"/>
        <source>ReloadAll</source>
        <translation>重新加载</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="336"/>
        <location filename="../qwosessionlist.cpp" line="341"/>
        <source>Add</source>
        <translation>增加</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="343"/>
        <source>SshConnect</source>
        <translation>SSH连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="346"/>
        <source>SftpConnect</source>
        <translation>SFTP连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="349"/>
        <source>TelnetConnect</source>
        <translation>TELNET连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="352"/>
        <source>RLoginConnect</source>
        <translation>RLOGIN连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="355"/>
        <source>MstscConnect</source>
        <translation>MSTSC连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="358"/>
        <source>VncConnect</source>
        <translation>VNC连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="361"/>
        <source>SerialConnect</source>
        <translation>串口连接</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="363"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../qwosessionlist.cpp" line="364"/>
        <location filename="../qwosessionlist.cpp" line="366"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>QWoSessionManage</name>
    <message>
        <location filename="../qwosessionmanage.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.ui" line="26"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.ui" line="43"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.ui" line="60"/>
        <location filename="../qwosessionmanage.cpp" line="271"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.ui" line="77"/>
        <location filename="../qwosessionmanage.cpp" line="487"/>
        <location filename="../qwosessionmanage.cpp" line="489"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="vanished">导入</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="40"/>
        <source>Session List</source>
        <translation>会话列表</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="76"/>
        <source>Enter keyword to search target quickly</source>
        <translation>关键字搜索</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="113"/>
        <location filename="../qwosessionmanage.cpp" line="132"/>
        <location filename="../qwosessionmanage.cpp" line="151"/>
        <location filename="../qwosessionmanage.cpp" line="170"/>
        <location filename="../qwosessionmanage.cpp" line="189"/>
        <location filename="../qwosessionmanage.cpp" line="208"/>
        <location filename="../qwosessionmanage.cpp" line="227"/>
        <location filename="../qwosessionmanage.cpp" line="239"/>
        <location filename="../qwosessionmanage.cpp" line="371"/>
        <location filename="../qwosessionmanage.cpp" line="381"/>
        <location filename="../qwosessionmanage.cpp" line="399"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="113"/>
        <location filename="../qwosessionmanage.cpp" line="132"/>
        <location filename="../qwosessionmanage.cpp" line="151"/>
        <location filename="../qwosessionmanage.cpp" line="170"/>
        <location filename="../qwosessionmanage.cpp" line="189"/>
        <location filename="../qwosessionmanage.cpp" line="208"/>
        <location filename="../qwosessionmanage.cpp" line="227"/>
        <location filename="../qwosessionmanage.cpp" line="239"/>
        <location filename="../qwosessionmanage.cpp" line="381"/>
        <location filename="../qwosessionmanage.cpp" line="399"/>
        <source>no selection</source>
        <translation>没有选中项</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="271"/>
        <source>No Selection</source>
        <translation>没有选中项</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="295"/>
        <source>Import File</source>
        <translation>导入文件</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="304"/>
        <location filename="../qwosessionmanage.cpp" line="320"/>
        <location filename="../qwosessionmanage.cpp" line="325"/>
        <location filename="../qwosessionmanage.cpp" line="336"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="304"/>
        <source>it&apos;s not a ssh config format: %1</source>
        <translation>文件格式错误：%1</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="320"/>
        <source>failed to find the identify file list in config file for bad path: %1</source>
        <translation>无法获取证书文件列表</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="325"/>
        <source>bad identify file: %1</source>
        <translation>错误的证书文件</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="336"/>
        <source>The config file contain the follow identify&apos;s files, Please export them before do this action.</source>
        <translation>配置文件中包括了相关证书文件，请先导出证书后，再导入该配置文件</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="371"/>
        <source>can&apos;t open over 6 session in same page.</source>
        <translation>同一个窗口内无法打开超过六个会话。</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="460"/>
        <location filename="../qwosessionmanage.cpp" line="464"/>
        <source>Add</source>
        <translation>增加</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="466"/>
        <source>SshConnect</source>
        <translation>SSH连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="469"/>
        <source>SftpConnect</source>
        <translation>SFTP连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="472"/>
        <source>TelnetConnect</source>
        <translation>TELNET连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="475"/>
        <source>RLoginConnect</source>
        <translation>RLOGIN连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="478"/>
        <source>MstscConnect</source>
        <translation>MSTSC连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="481"/>
        <source>VncConnect</source>
        <translation>VNC连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="484"/>
        <source>SerialConnect</source>
        <translation>串口连接</translation>
    </message>
    <message>
        <location filename="../qwosessionmanage.cpp" line="486"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
</context>
<context>
    <name>QWoSessionMoreProperty</name>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="42"/>
        <source>Color Schema</source>
        <translation>配色选项</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="62"/>
        <source>Schema</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="95"/>
        <source>Keyboard Layout</source>
        <translation>键盘布局</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="113"/>
        <source>Server Code Page</source>
        <translation>服务器编码</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="133"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="145"/>
        <source>Size:</source>
        <translation>字体大小：</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="174"/>
        <source>Font:</source>
        <translation>字体：</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="193"/>
        <source>Cursor</source>
        <translation>鼠标类型</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="201"/>
        <source>Block Cursor</source>
        <translation>BLOCK鼠标</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="208"/>
        <source>Underline Cursor</source>
        <translation>UNDERLINE鼠标</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="215"/>
        <source>IBeam Cursor</source>
        <translation>IBEAM鼠标</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="233"/>
        <source>Scroll Buffer</source>
        <translation>屏幕滚动行数</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="239"/>
        <source>BufferSize:</source>
        <translation>缓冲区大小</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="262"/>
        <source>lines</source>
        <translation>行数</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="309"/>
        <source>Max Protocol Version:</source>
        <translation>协议最高版本</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="327"/>
        <source>Pixel Format:</source>
        <translation>图像格式</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="345"/>
        <source>Encodings</source>
        <translation>编码</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="353"/>
        <source>H264</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="360"/>
        <source>JPEG</source>
        <translation>JPEG</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="367"/>
        <source>ZRLE3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="374"/>
        <source>TRLE3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="385"/>
        <source>ZRLE2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="395"/>
        <source>TRLE2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="405"/>
        <source>ZRLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="415"/>
        <source>TRLE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="429"/>
        <source>Hextile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="439"/>
        <source>Copy Rect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="449"/>
        <source>RRE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="462"/>
        <source>Raw</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="479"/>
        <source>Support Desktop Resize</source>
        <translation>支持桌面分辨率变更</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="489"/>
        <source>Disable local cursor and keyboard input</source>
        <translation>禁用本地鼠标和键盘输入</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="513"/>
        <source>Remote Desktop Size</source>
        <translation>远程桌面大小</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="521"/>
        <source>Local Desktop Size</source>
        <translation>本地桌面大小</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="533"/>
        <source>Fix Size</source>
        <translation>固定大小</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="556"/>
        <source>Width:</source>
        <translation>宽度：</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="573"/>
        <source>Height:</source>
        <translation>高度：</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="617"/>
        <location filename="../qwosessionmoreproperty.cpp" line="51"/>
        <location filename="../qwosessionmoreproperty.cpp" line="256"/>
        <source>Language</source>
        <translation>语言选项</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="629"/>
        <source>Restart application to take effect.</source>
        <translation>应用重启才生效</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="680"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.ui" line="687"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="37"/>
        <source>SesstionProperty</source>
        <translation>会话属性</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="43"/>
        <location filename="../qwosessionmoreproperty.cpp" line="250"/>
        <source>Terminal</source>
        <translation>SSH终端</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="44"/>
        <location filename="../qwosessionmoreproperty.cpp" line="254"/>
        <source>ShortCut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="47"/>
        <location filename="../qwosessionmoreproperty.cpp" line="252"/>
        <source>Rdp/Mstsc</source>
        <translation>RDP桌面</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="49"/>
        <source>Vnc</source>
        <translation>VNC桌面</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="493"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="494"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="495"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="496"/>
        <source>SelectAll</source>
        <translation>选择全部</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="497"/>
        <source>Select text from right to left</source>
        <translation>从右到左选择文本内容</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="498"/>
        <source>Select text from left to right</source>
        <translation>从左到右选择文本内容</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="499"/>
        <source>Select text from bottom to up</source>
        <translation>从下向上选择文本内容</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="500"/>
        <source>Select text from top to bottom</source>
        <translation>从上向下选择文本内容</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="501"/>
        <source>Select text from from line start to end</source>
        <translation>从行开始选择文本内容</translation>
    </message>
    <message>
        <location filename="../qwosessionmoreproperty.cpp" line="502"/>
        <source>Select text from current to line end</source>
        <translation>从当前至行尾选择文本内容</translation>
    </message>
</context>
<context>
    <name>QWoSessionProperty</name>
    <message>
        <location filename="../qwosessionproperty.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="22"/>
        <source>       Type:</source>
        <translation>类型:</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="43"/>
        <source>       Name:</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="59"/>
        <source>       Host:</source>
        <translation>主机名：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="69"/>
        <source>   Port:</source>
        <translation>端口：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="95"/>
        <location filename="../qwosessionproperty.cpp" line="287"/>
        <location filename="../qwosessionproperty.cpp" line="303"/>
        <source>Default:22</source>
        <translation>默认：22</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="106"/>
        <source>  LoginName:</source>
        <translation>登录名：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="120"/>
        <source>  LoginType:</source>
        <translation>登录类型：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="147"/>
        <source>   Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="164"/>
        <source>   Identify:</source>
        <translation>证书：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="189"/>
        <source>  ProxyJump:</source>
        <translation>跳板机:</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="214"/>
        <source>   Commands:</source>
        <translation>命令：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="232"/>
        <source>   BaudRate:</source>
        <translation>波特率：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="253"/>
        <source>   DataBits:</source>
        <translation>数据位：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="274"/>
        <source>     Parity:</source>
        <translation>校验位：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="295"/>
        <source>   StopBits:</source>
        <translation>停止位：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="316"/>
        <source>FlowControl:</source>
        <translation>流控协议：</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="337"/>
        <source>Memo</source>
        <translation>备忘</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="362"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="369"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="376"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.ui" line="383"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="94"/>
        <source>Session[New]</source>
        <translation>会话[新建]</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="108"/>
        <source>Session[%1]</source>
        <translation>会话[%1]</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="117"/>
        <location filename="../qwosessionproperty.cpp" line="171"/>
        <location filename="../qwosessionproperty.cpp" line="409"/>
        <location filename="../qwosessionproperty.cpp" line="475"/>
        <location filename="../qwosessionproperty.cpp" line="501"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="119"/>
        <location filename="../qwosessionproperty.cpp" line="172"/>
        <source>Identify</source>
        <translation>证书</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="319"/>
        <source>Default:23</source>
        <translation>默认：23</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="334"/>
        <source>Default:513</source>
        <translation>默认：513</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="349"/>
        <source>Default:3389</source>
        <translation>默认：3389</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="364"/>
        <source>Default:590x</source>
        <translation>默认：590x</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="480"/>
        <location filename="../qwosessionproperty.cpp" line="488"/>
        <location filename="../qwosessionproperty.cpp" line="492"/>
        <location filename="../qwosessionproperty.cpp" line="506"/>
        <location filename="../qwosessionproperty.cpp" line="511"/>
        <location filename="../qwosessionproperty.cpp" line="519"/>
        <location filename="../qwosessionproperty.cpp" line="523"/>
        <location filename="../qwosessionproperty.cpp" line="575"/>
        <location filename="../qwosessionproperty.cpp" line="580"/>
        <location filename="../qwosessionproperty.cpp" line="585"/>
        <location filename="../qwosessionproperty.cpp" line="591"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="480"/>
        <source>The identity file can&apos;t be empty</source>
        <translation>证书文件不能为空</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="506"/>
        <source>The identify file can&apos;t be empty</source>
        <translation>证书文件不能为空</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="511"/>
        <source>failed to find the identify file</source>
        <translation>无法查到证书文件</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="488"/>
        <location filename="../qwosessionproperty.cpp" line="519"/>
        <source>ProxyJump can&apos;t be same with name, change to another one.</source>
        <translation>名字不能与跳板机名字相同，请更换其它跳板机名字</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="492"/>
        <location filename="../qwosessionproperty.cpp" line="523"/>
        <source>The userName can&apos;t be empty</source>
        <translation>登录名不能为空</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="575"/>
        <source>The name can&apos;t be empty</source>
        <translation>会话名不能为空</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="580"/>
        <source>The host can&apos;t be empty</source>
        <translation>主机名不能为空</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="585"/>
        <source>The port should be at [10,65535]</source>
        <translation>端口取值范围必须是[10,65535]之间</translation>
    </message>
    <message>
        <location filename="../qwosessionproperty.cpp" line="591"/>
        <source>The session name had been used, Change to another name.</source>
        <translation>会话名已经被占用，请更换其它名字</translation>
    </message>
</context>
<context>
    <name>QWoSettingDialog</name>
    <message>
        <location filename="../qwosettingdialog.ui" line="14"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qwosettingdialog.ui" line="24"/>
        <source>Create desktop entry</source>
        <translation>创建桌面入口</translation>
    </message>
</context>
<context>
    <name>QWoSftpDownMgrModel</name>
    <message>
        <location filename="../qwosftpdownmgrmodel.cpp" line="51"/>
        <source>Name</source>
        <translation>会话名字</translation>
    </message>
    <message>
        <location filename="../qwosftpdownmgrmodel.cpp" line="53"/>
        <source>Host</source>
        <translation>主机名</translation>
    </message>
    <message>
        <location filename="../qwosftpdownmgrmodel.cpp" line="55"/>
        <source>Memo</source>
        <translation>备忘</translation>
    </message>
</context>
<context>
    <name>QWoSftpListModel</name>
    <message>
        <location filename="../qwosftplistmodel.cpp" line="64"/>
        <source>Name</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../qwosftplistmodel.cpp" line="66"/>
        <source>Owner</source>
        <translation>拥有者</translation>
    </message>
    <message>
        <location filename="../qwosftplistmodel.cpp" line="68"/>
        <source>Group</source>
        <translation>组名</translation>
    </message>
    <message>
        <location filename="../qwosftplistmodel.cpp" line="70"/>
        <source>Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../qwosftplistmodel.cpp" line="72"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
</context>
<context>
    <name>QWoSftpNameDialog</name>
    <message>
        <location filename="../qwosftpnamedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../qwosftpnamedialog.ui" line="28"/>
        <source>Name:</source>
        <translation>会话名：</translation>
    </message>
    <message>
        <location filename="../qwosftpnamedialog.ui" line="55"/>
        <source>Private</source>
        <translation>私有</translation>
    </message>
    <message>
        <location filename="../qwosftpnamedialog.ui" line="92"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qwosftpnamedialog.ui" line="99"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qwosftpnamedialog.cpp" line="25"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
</context>
<context>
    <name>QWoSftpTransferWidget</name>
    <message>
        <location filename="../qwosftptransferwidget.ui" line="14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location filename="../qwosftptransferwidget.ui" line="84"/>
        <source>Progress</source>
        <translation>进度</translation>
    </message>
    <message>
        <location filename="../qwosftptransferwidget.ui" line="101"/>
        <source>file</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../qwosftptransferwidget.ui" line="136"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
</context>
<context>
    <name>QWoSftpWidget</name>
    <message>
        <location filename="../qwosftpwidget.ui" line="14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="247"/>
        <location filename="../qwosftpwidget.cpp" line="255"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="248"/>
        <location filename="../qwosftpwidget.cpp" line="256"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="249"/>
        <location filename="../qwosftpwidget.cpp" line="257"/>
        <source>Home Directory</source>
        <translation>返回主文件夹</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="250"/>
        <location filename="../qwosftpwidget.cpp" line="258"/>
        <source>Create Directory</source>
        <translation>创建文件夹</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="251"/>
        <location filename="../qwosftpwidget.cpp" line="268"/>
        <source>Upload</source>
        <translation>上传</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="260"/>
        <source>Remove Directory</source>
        <translation>删除文件夹</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="261"/>
        <source>Enter</source>
        <translation>进入</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="263"/>
        <source>Remove File</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="264"/>
        <source>Download</source>
        <translation>下载</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="266"/>
        <source>Try Enter</source>
        <translation>尝度进入</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="270"/>
        <source>New Session Multiplex</source>
        <translation>新建连接复用</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="271"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="295"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="428"/>
        <source>SaveFile</source>
        <translation>保存文件</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="439"/>
        <source>FileExist</source>
        <translation>文件存在</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="440"/>
        <source>has the same name in the target path. override it?</source>
        <translation>已经存在同名文件，是否覆盖？</translation>
    </message>
    <message>
        <location filename="../qwosftpwidget.cpp" line="453"/>
        <source>Select File</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>QWoSftpWidgetImpl</name>
    <message>
        <location filename="../qwosftpwidgetimpl.cpp" line="73"/>
        <source>New Session Multiplex</source>
        <translation>新建连接复用</translation>
    </message>
</context>
<context>
    <name>QWoShortCutModel</name>
    <message>
        <location filename="../qwoshortcutmodel.cpp" line="130"/>
        <source>Desc</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../qwoshortcutmodel.cpp" line="132"/>
        <source>Key</source>
        <translation>快捷键</translation>
    </message>
</context>
<context>
    <name>QWoShower</name>
    <message>
        <location filename="../qwoshower.cpp" line="291"/>
        <source>Close Or Not</source>
        <translation>是否关闭</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="293"/>
        <source>The follow event will be stop before close this session.<byte value="xd"/>
<byte value="xd"/>
</source>
        <translation>关闭会话的同时，也停止以下相应行为</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="297"/>
        <source><byte value="xd"/>
<byte value="xd"/>
Continue To Close It?</source>
        <translation>继续关闭</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="299"/>
        <source>CloseSession</source>
        <translation>关闭会话</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="332"/>
        <source>Close This Tab</source>
        <translation>关闭此选项卡</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="333"/>
        <source>Close Other Tab</source>
        <translation>关闭其它选项卡</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="334"/>
        <source>Float This Tab</source>
        <translation>浮动此选项卡</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="390"/>
        <location filename="../qwoshower.cpp" line="404"/>
        <source>alert</source>
        <translation>告警</translation>
    </message>
    <message>
        <location filename="../qwoshower.cpp" line="390"/>
        <location filename="../qwoshower.cpp" line="404"/>
        <source>failed to find impl infomation</source>
        <translation>无法查找目标信息</translation>
    </message>
</context>
<context>
    <name>QWoSshConf</name>
    <message>
        <source>LoadSshConfig</source>
        <translation type="vanished">加载配置</translation>
    </message>
    <message>
        <source>Failed to open file:</source>
        <translation type="vanished">无法打开文件</translation>
    </message>
</context>
<context>
    <name>QWoSshTermWidget</name>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="155"/>
        <source>Reconnection confirmation</source>
        <translation>重连确认</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="155"/>
        <source>Continue to connect to the server?</source>
        <translation>继续连接远程服务器？</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="210"/>
        <location filename="../qwosshtermwidget.cpp" line="221"/>
        <source>session list</source>
        <translation>会话列表</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="258"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="258"/>
        <source>can&apos;t find the session, maybe it had been delete ago</source>
        <translation>无法找到相应会话信息</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="282"/>
        <source>Select Files</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="301"/>
        <source>Open Directory</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="480"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="482"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="484"/>
        <source>Force Reconnect</source>
        <translation>强制重新联网</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="485"/>
        <source>Split Vertical</source>
        <translation>垂直分割</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="487"/>
        <source>Split Horizontal</source>
        <translation>水平分割</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="493"/>
        <source>Sftp Assistant</source>
        <translation>SFTP助手</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="495"/>
        <source>Find...</source>
        <translation>查找...</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="496"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="498"/>
        <source>Duplicate In New Window</source>
        <translation>在新窗口中打开</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="499"/>
        <source>New Session Multiplex</source>
        <translation>新建连接复用</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="500"/>
        <source>Clean history</source>
        <translation>清空历史</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="501"/>
        <source>Output history to file</source>
        <translation>将历史输出至文件</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="502"/>
        <source>Stop history to file</source>
        <translation>停止输出历史文件</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="503"/>
        <source>Zmodem Upload</source>
        <translation>ZModem上传</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="504"/>
        <source>Zmodem Receive</source>
        <translation>ZModem下载</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="505"/>
        <source>Zmoddem Abort</source>
        <translation>ZModem中断传输</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="506"/>
        <source>Close Session</source>
        <translation>关闭会话</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="509"/>
        <source>Float This Tab</source>
        <translation>浮动此选项卡</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidget.cpp" line="529"/>
        <source>Files are transfering...</source>
        <translation>文件正在传输中...</translation>
    </message>
</context>
<context>
    <name>QWoSshTermWidgetImpl</name>
    <message>
        <location filename="../qwosshtermwidgetimpl.cpp" line="37"/>
        <source>%1:%2</source>
        <translation>%1:%2</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidgetimpl.cpp" line="47"/>
        <source>Sftp Assistant</source>
        <translation>SFTP助手</translation>
    </message>
    <message>
        <location filename="../qwosshtermwidgetimpl.cpp" line="48"/>
        <source>New Session Multiplex</source>
        <translation>新建连接复用</translation>
    </message>
</context>
<context>
    <name>QWoTelnetTermWidget</name>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="138"/>
        <source>Reconnection confirmation</source>
        <translation>连接确认</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="138"/>
        <source>Continue to connect to the server?</source>
        <translation>继续连接服务器？</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="193"/>
        <location filename="../qwotelnettermwidget.cpp" line="204"/>
        <source>session list</source>
        <translation>会话列表</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="235"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="235"/>
        <source>can&apos;t find the session, maybe it had been delete ago</source>
        <translation>无法查找相应会话</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="254"/>
        <source>Select Files</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="273"/>
        <source>Open Directory</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="399"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="401"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="402"/>
        <source>Force Reconnect</source>
        <translation>强制重新联网</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="404"/>
        <source>Split Vertical</source>
        <translation>垂直分割</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="406"/>
        <source>Split Horizontal</source>
        <translation>水平分割</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="413"/>
        <source>Close Session</source>
        <translation>关闭会话</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="417"/>
        <source>Float This Tab</source>
        <translation>浮动此选项卡</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="420"/>
        <source>Find...</source>
        <translation>查找...</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="421"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="423"/>
        <source>Duplicate In New Window</source>
        <translation>用新窗口中复制会话</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="424"/>
        <source>Clean history</source>
        <translation>清空历史</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="425"/>
        <source>Output history to file</source>
        <translation>将历史输出至文件</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="426"/>
        <source>Stop history to file</source>
        <translation>停止输出历史文件</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="428"/>
        <source>Zmodem Upload</source>
        <translation>ZModem上传</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="429"/>
        <source>Zmodem Receive</source>
        <translation>ZModem下载</translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="430"/>
        <source>Zmoddem Abort</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwotelnettermwidget.cpp" line="451"/>
        <source>Files are transfering...</source>
        <translation>文件正在传输中...</translation>
    </message>
</context>
<context>
    <name>QWoTelnetTermWidgetImpl</name>
    <message>
        <location filename="../qwotelnettermwidgetimpl.cpp" line="31"/>
        <source>%1:%2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QWoTermMask</name>
    <message>
        <location filename="../qwotermmask.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwotermmask.ui" line="48"/>
        <source>Reconect</source>
        <translation>重新连接</translation>
    </message>
    <message>
        <location filename="../qwotermmask.ui" line="81"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>QWoTermWidget</name>
    <message>
        <location filename="../qwotermwidget.cpp" line="181"/>
        <source>Save history to file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwotermwidget.cpp" line="181"/>
        <source>log (*.log)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwotermwidget.cpp" line="187"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../qwotermwidget.cpp" line="187"/>
        <source>Failed to create file.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QWoVncFtpWidget</name>
    <message>
        <location filename="../qwovncftpwidget.ui" line="14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location filename="../qwovncftpwidget.ui" line="44"/>
        <source>Ftp</source>
        <translation>FTP</translation>
    </message>
    <message>
        <location filename="../qwovncftpwidget.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QWoVncPlayWidget</name>
    <message>
        <location filename="../qwovncplaywidget.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QWoVncToolForm</name>
    <message>
        <location filename="../qwovnctoolform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qwovnctoolform.ui" line="35"/>
        <location filename="../qwovnctoolform.ui" line="52"/>
        <location filename="../qwovnctoolform.ui" line="69"/>
        <location filename="../qwovnctoolform.ui" line="86"/>
        <location filename="../qwovnctoolform.ui" line="103"/>
        <location filename="../qwovnctoolform.ui" line="120"/>
        <location filename="../qwovnctoolform.ui" line="137"/>
        <location filename="../qwovnctoolform.ui" line="154"/>
        <location filename="../qwovnctoolform.ui" line="171"/>
        <location filename="../qwovnctoolform.ui" line="188"/>
        <location filename="../qwovnctoolform.ui" line="205"/>
        <location filename="../qwovnctoolform.ui" line="222"/>
        <location filename="../qwovnctoolform.ui" line="239"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QWoVncWidget</name>
    <message>
        <location filename="../qwovncwidget.cpp" line="235"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qwovncwidget.cpp" line="235"/>
        <source>Please input the right password.</source>
        <translation>请输入正确的密码</translation>
    </message>
</context>
<context>
    <name>qsftp</name>
    <message>
        <location filename="../qml/qsftp.qml" line="41"/>
        <source>upload:</source>
        <translation>上传：</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="50"/>
        <source>download:</source>
        <translation>下载：</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="233"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="242"/>
        <source>Reflesh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="251"/>
        <location filename="../qml/qsftp.qml" line="402"/>
        <source>Create Directory</source>
        <translation>创建文件夹</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="260"/>
        <source>Remove Directory</source>
        <translation>删除文件夹</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="269"/>
        <source>Remove File</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="278"/>
        <source>Enter</source>
        <translation>进入</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="287"/>
        <source>TryEnter</source>
        <translation>尝试进入</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="296"/>
        <source>Download</source>
        <translation>下载</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="305"/>
        <source>Upload</source>
        <translation>上传</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="418"/>
        <source>Private</source>
        <translation>私有</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="433"/>
        <source>Name:</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="441"/>
        <source>Enter Directory Name</source>
        <translation>进入目录名</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="453"/>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="464"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="510"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="538"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="576"/>
        <source>Progress</source>
        <translation>进度</translation>
    </message>
    <message>
        <location filename="../qml/qsftp.qml" line="608"/>
        <source>Abort</source>
        <translation>关于</translation>
    </message>
</context>
</TS>
